# biletado-schreiner-wagner-d7b3906a-dce0-11ec-9d64-0242ac120002



## Getting started

- Install "biletado-compose" from https://gitlab.com/tomwgnr/biletado-compose/
- Build with "docker-compose build"
- Run with "docker-compose up"

Default port is :8081

## Using the API

- Send POST request to /authenticate with contents:
    
    {
    "username": "foo",
    "password": "bar"
    }

- Copy the JWT of the response and add the header "Authorization": "Bearer \<your-token\>" in the following requests.
- Use the API as usual
