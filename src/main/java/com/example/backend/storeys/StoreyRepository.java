package com.example.backend.storeys;

import com.example.backend.rooms.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface StoreyRepository extends JpaRepository<Storey, UUID> {

    @Query("SELECT b FROM Room b WHERE b.storey.id = :storeyid")
    List<Room> findRoomByStorey(@Param("storeyid") UUID storeyid);

}
