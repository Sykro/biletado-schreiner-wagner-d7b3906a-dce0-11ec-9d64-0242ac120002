package com.example.backend.storeys;

import com.example.backend.buildings.Buildings;
import com.example.backend.buildings.BuildingsService;
import com.example.backend.requests.StoreyRequest;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class StoreyController {

    private final StoreyService storeyService;
    private final BuildingsService buildingsService;

    @Autowired
    public StoreyController(StoreyService storeyService, BuildingsService buildingsService) {
        this.storeyService = storeyService;
        this.buildingsService = buildingsService;
    }

    @GetMapping("/assets/storeys")
    public List<Storey> getStoreys() {
        return storeyService.getStoreys();
    }


    @PostMapping("/assets/storeys")
    public ResponseEntity<?> addStorey(@RequestBody StoreyRequest request) {
        Storey storey = getStoreyFromRequest(request);
        System.out.println(storey);
        if (storey.getBuilding() == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Building not found");
        }
        if (storey.getId() == null || storeyService.storeyExists(storey.getId())){
            storeyService.addOrUpdateStorey(storey);
            return ResponseEntity.status(HttpStatus.OK).body("OK");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Storey not found");
    }

    @GetMapping("/assets/storeys/{id}")
    public ResponseEntity<?> getStoreyById(@PathVariable UUID id) {
        Optional<Storey> storey = storeyService.findById(id);
        if (storey.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(storey);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PutMapping("/assets/storeys/{id}")
    public ResponseEntity<?> updateStorey(@RequestBody StoreyRequest request, @PathVariable UUID id) {
        Storey storey = getStoreyFromRequest(request);
        if (storey.getId() == null){
            storey.setId(id);
        }
        System.out.println(storey);
        if (storey.getId() != id) {
            return ResponseEntity.status(422).body("Mismatching ID");
        }
        storeyService.addOrUpdateStorey(storey);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/assets/storeys/{id}")
    public ResponseEntity<?> deleteStorey(@PathVariable UUID id) {
        if (!storeyService.storeyExists(id)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
        boolean room = storeyService.findRoom(id);
        System.out.println(room);
        if (room){
            return ResponseEntity.status(422).body("deletion not possible because of existing rooms");
        }
        storeyService.deleteStorey(id);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/assets/storeys/deleteall")
    public void deleteAll(){
        storeyService.deleteAll();
    }

    private Storey getStoreyFromRequest(StoreyRequest request) {
        Storey storey = new Storey();
        storey.setName(request.getName());
        storey.setId(request.getId());
        Optional<Buildings> building = buildingsService.findById(request.getBuilding_id());
        building.ifPresent(storey::setBuilding);
        return storey;
    }

}
