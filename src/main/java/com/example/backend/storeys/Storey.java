package com.example.backend.storeys;

import com.example.backend.buildings.Buildings;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="storeys")
public class Storey {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @ManyToOne
    @JoinColumn(name= "building_id")
    private Buildings building;
    @Column
    private String name;

    public Storey() {
    }

    public Storey(UUID id, Buildings building, String name) {
        this.id = id;
        this.building = building;
        this.name = name;
    }

    public Storey(Buildings building, String name) {
        this.building = building;
        this.name = name;
    }

//    public Storey(UUID id, UUID building_id, String name){
//        this.id = id;
//        this.building = buildingRepository
//    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Buildings getBuilding() {
        return building;
    }

    public void setBuilding(Buildings building) {
        this.building = building;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Storey{" +
                "id=" + id +
                ", Building=" + building + '\'' +
                ", name=" + name +
                '}';
    }
}
