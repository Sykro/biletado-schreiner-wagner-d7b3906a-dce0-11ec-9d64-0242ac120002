package com.example.backend.storeys;

import com.example.backend.rooms.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class StoreyService {
    
    private final StoreyRepository storeyRepository;
    
    @Autowired
    public StoreyService(StoreyRepository storeyRepository){
        this.storeyRepository = storeyRepository;
    }
    
    public List<Storey> getStoreys(){ return storeyRepository.findAll();
    }

    public void addOrUpdateStorey(Storey storey) {
        storeyRepository.save(storey);
    }

    public Optional<Storey> findById(UUID id) {
        return storeyRepository.findById(id);
    }

    public boolean storeyExists(UUID id) {
        return storeyRepository.existsById(id);
    }

    public void deleteStorey(UUID id) {
        storeyRepository.deleteById(id);
    }

    public boolean findRoom(UUID id){
        List<Room> test = storeyRepository.findRoomByStorey(id);
        System.out.println("TEst ist: " + test);
        if(test.isEmpty()){
            return false;
        }
        return true;
    }

    public void deleteAll() {
        storeyRepository.deleteAll();
    }
}
