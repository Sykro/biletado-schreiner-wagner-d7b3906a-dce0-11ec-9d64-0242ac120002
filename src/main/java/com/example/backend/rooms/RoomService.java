package com.example.backend.rooms;


import com.example.backend.buildings.BuildingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    @Autowired public RoomService(RoomRepository roomRepository){
        this.roomRepository = roomRepository;
    }

    public List<Room> getRooms(){
        return roomRepository.findAll();
    }

    public void addOrUpdateRoom(Room room) {
        roomRepository.save(room);
    }


    public Optional<Room> findById(UUID id) {
        return roomRepository.findById(id);
    }

    public boolean roomExists(UUID id) {
        return roomRepository.existsById(id);
    }

    public void deleteRoom(UUID id) {
        roomRepository.deleteById(id);
    }

    public void deleteAll() {
        roomRepository.deleteAll();
    }
}
