package com.example.backend.rooms;

import com.example.backend.requests.RoomRequest;
import com.example.backend.storeys.Storey;
import com.example.backend.storeys.StoreyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class RoomController {

    private final RoomService roomService;
    private final StoreyService storeyService;

    @Autowired
    public RoomController(RoomService roomService, StoreyService storeyService){
        this.roomService = roomService;
        this.storeyService = storeyService;
    }

    @GetMapping("/assets/rooms")
    public List<Room> getRooms() {return roomService.getRooms();}

    @PostMapping("/assets/rooms")
    public ResponseEntity<?> addRoom(@RequestBody RoomRequest request){
        Room room = getRoomFromRequest(request);
        System.out.println(room);
        if (room.getStorey() == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Storey not found");
        }
        if (room.getId() == null || roomService.roomExists(room.getId())){
            roomService.addOrUpdateRoom(room);
            return ResponseEntity.status(HttpStatus.OK).body("OK");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Room not found");
    }

    @GetMapping("/assets/rooms/{id}")
    public ResponseEntity<?> getRoomById(@PathVariable UUID id){
        Optional<Room> room = roomService.findById(id);
        if(room.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(room);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PutMapping("/assets/rooms/{id}")
    public ResponseEntity<?> updateRoom(@RequestBody RoomRequest request, @PathVariable UUID id){
        Room room = getRoomFromRequest(request);
        System.out.println(room);
        if (room.getId() == null){
            room.setId(id);
        }
        if(room.getId() != id){
            return ResponseEntity.status(422).body("Mismatching ID");
        }
        roomService.addOrUpdateRoom(room);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/assets/rooms/{id}")
    public ResponseEntity<?> deleteRoom(@PathVariable UUID id) {
        if(!roomService.roomExists(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
        }
        roomService.deleteRoom(id);
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/assets/rooms/deleteall")
    public void deleteAll(){
        roomService.deleteAll();
    }

    private Room getRoomFromRequest(RoomRequest request) {
        Room room = new Room();
        room.setName(request.getName());
        room.setId(request.getId());
        Optional<Storey> storey = storeyService.findById(request.getStorey_id());
        storey.ifPresent(room::setStorey);
        return room;
    }

}
