package com.example.backend.rooms;

import com.example.backend.storeys.Storey;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="rooms")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @ManyToOne
    @JoinColumn(name="storey_id")
    private Storey storey;
    @Column
    private String name;

    public Room(Storey storey, String name) {
        this.storey = storey;
        this.name = name;
    }

    public Room(UUID id, Storey storey, String name) {
        this.id = id;
        this.storey = storey;
        this.name = name;
    }

    public Room() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Storey getStorey() {
        return storey;
    }

    public void setStorey(Storey storey) {
        this.storey = storey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name=" + name + '\'' +
                ", storey=" + storey +
                '}';
    }

}
