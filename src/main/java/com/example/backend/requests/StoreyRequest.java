package com.example.backend.requests;

import java.util.UUID;

public class StoreyRequest {
    private UUID id;
    private UUID building_id;
    private String name;

    public StoreyRequest(UUID id, UUID building_id, String name) {
        this.id = id;
        this.building_id = building_id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(UUID building_id) {
        this.building_id = building_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
