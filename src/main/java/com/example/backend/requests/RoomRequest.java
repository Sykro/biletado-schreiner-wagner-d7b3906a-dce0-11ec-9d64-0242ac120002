package com.example.backend.requests;

import java.util.UUID;

public class RoomRequest {
    private UUID id;
    private UUID storey_id;
    private String name;

    public RoomRequest(UUID id, UUID storey_id, String name) {
        this.id = id;
        this.storey_id = storey_id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getStorey_id() {
        return storey_id;
    }

    public void setStorey_id(UUID storey_id) {
        this.storey_id = storey_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
