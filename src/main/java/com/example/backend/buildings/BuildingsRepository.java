package com.example.backend.buildings;

import com.example.backend.rooms.Room;
import com.example.backend.storeys.Storey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface BuildingsRepository extends JpaRepository<Buildings, UUID> {

    @Query("SELECT b FROM Storey b WHERE b.building.id = :buildingid")
    List<Storey> findStoreyByBuilding(@Param("buildingid") UUID buildingid);

}
