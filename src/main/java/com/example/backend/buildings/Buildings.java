package com.example.backend.buildings;


import com.example.backend.storeys.Storey;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name="buildings")
public class Buildings {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @Column
    private String name;
    @Column
    private String address;
    @OneToMany
    private List<Storey> storeys;

    public Buildings(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public Buildings(UUID id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Buildings() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Building{" +
                "id=" + id +
                ", nane=" + name + '\'' +
                ", address=" + address +
                '}';
    }
}
