package com.example.backend.buildings;

import com.example.backend.storeys.Storey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BuildingsService {

    private final BuildingsRepository buildingsRepository;

    @Autowired
    public BuildingsService(BuildingsRepository buildingsRepository) {
        this.buildingsRepository = buildingsRepository;
    }

    public List<Buildings> getBuildings() {
        return buildingsRepository.findAll();
    }

    public void addNewBuilding(Buildings building){
        System.out.println(building);
        buildingsRepository.save(building);
    }

    public void updateBuilding(Buildings building){
        System.out.println(building);
        buildingsRepository.save(building);
    }

    public Optional<Buildings> findById(UUID id) {
        return buildingsRepository.findById(id);
    }

    public void deleteBuilding(UUID id) {
            buildingsRepository.deleteById(id);
    }

    public boolean findStoreyWithBuilding(UUID buildingid){
        List<Storey> storey = buildingsRepository.findStoreyByBuilding(buildingid);
        if(storey == null){
            return false;
        }
        return true;
    }

    public void deleteAll() {
        buildingsRepository.deleteAll();
    }

    public boolean existsById(UUID id) {
        return buildingsRepository.existsById(id);
    }
}
