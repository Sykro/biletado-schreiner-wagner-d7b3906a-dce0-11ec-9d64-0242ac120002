package com.example.backend.buildings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class BuildingsController {

    private final BuildingsService buildingsService;

    @Autowired
    public BuildingsController(BuildingsService buildingsService){
        this.buildingsService = buildingsService;
    }

    @GetMapping("/assets/buildings")
    public List<Buildings> getBuildings() {return buildingsService.getBuildings();}

    @PostMapping("/assets/buildings")
    public void addBuilding(@RequestBody Buildings building){
        System.out.println(building);
        buildingsService.addNewBuilding(building);
        System.out.println(building);

    }

    @GetMapping("/assets/buildings/{id}")
    public ResponseEntity<?> getBuildingById(@PathVariable UUID id){
        Optional<Buildings> building = buildingsService.findById(id);
        if(building != null){
            return ResponseEntity.status(HttpStatus.OK).body(building);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }

    @PutMapping("/assets/buildings/{id}")
    public ResponseEntity<?> putBuilding(@RequestBody Buildings building, @PathVariable UUID id){
        System.out.println(building);
        if(building.getId() != null && building.getId() != id){
            return ResponseEntity.status(422).body( "Mismatching ID");
        }
        else{
            buildingsService.updateBuilding(building);
        }
        return ResponseEntity.status(HttpStatus.OK).body("");
    }

    @DeleteMapping("/assets/buildings/{id}")
    public ResponseEntity<?> deleteBuilding(@PathVariable UUID id) throws ResponseStatusException {
        if (!buildingsService.existsById(id)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND");
        }
        boolean storeyExists = buildingsService.findStoreyWithBuilding(id);
        if (storeyExists){
            return ResponseEntity.status(422).body("deletion not possible because of existing storeys");
        }
        buildingsService.deleteBuilding(id);
        return ResponseEntity.status(HttpStatus.OK).body("OK");
        //return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/assets/buildings/deleteall")
    public void deleteAll(){
        buildingsService.deleteAll();
    }

}
