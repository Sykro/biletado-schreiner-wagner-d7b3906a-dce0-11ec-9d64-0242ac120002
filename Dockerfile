#FROM openjdk:17-alpine
##ARG JAR_FILE=target/*.jar
#WORKDIR /app
#COPY /target/Backend-0.0.1-SNAPSHOT.jar /app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]


FROM maven:3.8.4-openjdk-17 as build
WORKDIR /app
COPY ./ ./
RUN mvn clean install -DskipTests

FROM openjdk:17
WORKDIR /app
COPY --from=build /app/target/Backend-0.0.1-SNAPSHOT.jar /app
EXPOSE 8081
CMD ["java", "-jar", "Backend-0.0.1-SNAPSHOT.jar"]


#FROM openjdk:17
#WORKDIR /app
#ADD target/Backend-0.0.1-SNAPSHOT.jar Backend-0.0.1-SNAPSHOT.jar
#EXPOSE 8081
#ENTRYPOINT ["java", "-jar", "Backend-0.0.1-SNAPSHOT.jar"]